﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BowlingKata
{
    [TestClass]
    public class ScoringTests
    {
        [TestMethod]
        public void Score_Should_Sum_A_Frame()
        {
            var sut = new Score();

            var frame = new Frame();
            frame.Roll(new Roll(3));
            frame.Roll(new Roll(5));

            Assert.AreEqual(8, sut.GetTotal(new List<Frame>() {frame}));
        }

        [TestMethod]
        public void Score_Should_Sum_Two_Frames_With_A_Spare()
        {
            var sut = new Score();

            var frame1 = new Frame();
            frame1.Roll(new Roll(3));
            frame1.Roll(new Roll(7));

            var frame2 = new Frame();
            frame2.Roll(new Roll(3));
            frame2.Roll(new Roll(5));

            Assert.AreEqual(13 + 8, sut.GetTotal(new List<Frame>() { frame1, frame2 }));
        }

        [TestMethod]
        public void Score_Should_Sum_Two_Frames_With_A_Strike()
        {
            var sut = new Score();

            var frame1 = new Frame();
            frame1.Roll(new Roll(10));

            var frame2 = new Frame();
            frame2.Roll(new Roll(3));
            frame2.Roll(new Roll(1));

            Assert.AreEqual(14 + 4, sut.GetTotal(new List<Frame>() { frame1, frame2 }));
        }

        [TestMethod]
        public void Score_Should_Sum_Three_Frames_With_A_Strike()
        {
            var sut = new Score();

            var frame1 = new Frame();
            frame1.Roll(new Roll(10));

            var frame2 = new Frame();
            frame2.Roll(new Roll(10));

            var frame3 = new Frame();
            frame3.Roll(new Roll(3));
            frame3.Roll(new Roll(1));

            Assert.AreEqual(23 + 14 + 4, sut.GetTotal(new List<Frame>() { frame1, frame2, frame3 }));
        }

        [TestMethod]
        public void Score_Should_Return_Zero_When_There_Are_No_Rolls()
        {
            var sut = new Frame();

            Assert.AreEqual(0, sut.Score(new List<Frame>()));
        }
    }
}