﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingKata
{
    public class Game
    {
        private List<Frame> Frames = new List<Frame>(10);
        public Game()
        {
            Frame = 1;

            for (int i = 0; i < 9; i++)
            {
                Frames.Add(new Frame());
            }
            Frames.Add(new TenthFrame());
        }

        public void Roll(int pins)
        {
            var roll = new Roll(pins);
            if (Frame == 10)
            {
                ((TenthFrame)Frames[Frame - 1]).Roll(roll);
            }
            else
            {
                Frames[Frame - 1].Roll(roll);
            }

            if (Frames[Frame - 1].Closed)
            {
                Frame++;
            }
        }

        public int Frame { get; private set; }

        public bool GameOver { get { return Frames[10 - 1].Closed; } }

        public void PlayGame()
        {
            Score scorer = new Score();

            while (!GameOver)
            {
                Console.WriteLine("Enter Roll");
                int pins = int.Parse(Console.ReadLine());

                Roll(pins);

                Console.WriteLine("In frame " + Frame + ", your score is " + scorer.GetTotal(Frames));
            }

            Console.WriteLine("Game complete, your score is " + scorer.GetTotal(Frames));

            Console.WriteLine("Hit Enter to exit");
            Console.ReadLine();
        }
    }
}