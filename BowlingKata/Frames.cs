﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingKata
{
    public class Frame
    {
        protected List<Roll> _rolls = new List<Roll>(3);

        public void Roll(Roll roll)
        {
            if (Closed)
            {
                throw new Exception("Frame is closed!");
            }

            _rolls.Add(roll);
            if (_rolls.Count == 2 || roll.IsStrike)
            {
                Closed = true;
            }
        }

        public bool IsSpare { get { return _rolls.Take(2).Sum(r => r.Pins) == 10; } }
        public bool Closed { get; protected set; }
        public int Pins { get { return _rolls.Sum(r => r.Pins); } }

        public int Score(IEnumerable<Frame> followingFrames)
        {
            if (_rolls.Count == 0)
            {
                return 0;
            }
            var nextRolls = followingFrames.SelectMany(f => f._rolls).Select(r => r.Pins);

            if (_rolls[0].IsStrike)
            {
                return Pins + nextRolls.Take(2).Sum();
            }

            if (IsSpare)
            {
                return Pins + nextRolls.FirstOrDefault();
            }

            return Pins;
        }
    }

    public class TenthFrame : Frame
    {
        public new void Roll(Roll roll)
        {
            if (Closed)
            {
                throw new Exception("Frame is closed!");
            }

            if (_rolls.Count == 3)
            {
                throw new Exception("Too many rolls!");
            }

            _rolls.Add(roll);

            // Close at 3 rolls no matter what
            if (_rolls.Count == 3)
            {
                Closed = true;
                return;
            }

            if (_rolls[0].IsStrike && _rolls.Count < 3)
            {
                return;
            }

            if (_rolls.Count() == 2 && !IsSpare)
            {
                Closed = true;
            }
        }
    }
}
