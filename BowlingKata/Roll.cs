﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingKata
{
    public class Roll
    {
        private int _pins;

        public Roll(int pins)
        {
            _pins = pins;
        }

        public int Pins { get { return _pins; } }
        public bool IsStrike { get { return _pins == 10; } }
    }   
}