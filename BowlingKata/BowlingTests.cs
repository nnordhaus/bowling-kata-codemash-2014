﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BowlingKata
{
    [TestClass]
    public class BowlingTests
    {
        [TestMethod]
        public void Roll_Should_Set_IsStrike_When_Roll_is_10()
        {
            var sut = new Roll(10);

            Assert.IsTrue(sut.IsStrike);
        }

        [TestMethod]
        public void Frame_Should_Set_IsSpare_When_Both_Rolls_Sum_To_10()
        {
            var sut = new Frame();

            sut.Roll(new Roll(5));
            sut.Roll(new Roll(5));

            Assert.IsTrue(sut.IsSpare);
        }

        [TestMethod]
        public void Frame_Should_Close_Itself_After_2nd_Roll()
        {
            var sut = new Frame();
        
            sut.Roll(new Roll(1));
            sut.Roll(new Roll(2));
        
            Assert.IsTrue(sut.Closed);
        }

        [TestMethod]
        public void Frame_Should_Close_Itself_After_Strike()
        {
            var sut = new Frame();

            sut.Roll(new Roll(10));

            Assert.IsTrue(sut.Closed);
        }

        [TestMethod]
        public void Game_Should_Start_On_Frame_1()
        {
            var sut = new Game();

            Assert.AreEqual(1, sut.Frame);
        }
        
        [TestMethod]
        public void Game_Should_Close_Frame_On_Strike()
        {
            var sut = new Game();
        
            sut.Roll(10);
        
            Assert.AreEqual(2, sut.Frame);
        }

        [TestMethod]
        public void TenthFrame_Should_Close_After_2_Non_Strike_Rolls()
        {
            var sut = new TenthFrame();
        
            sut.Roll(new Roll(4));
            sut.Roll(new Roll(3));
        
            Assert.IsTrue(sut.Closed);
        }

        [TestMethod]
        public void TenthFrame_Should_Close_After_2_Non_Special_Rolls()
        {
            var sut = new TenthFrame();

            sut.Roll(new Roll(2));
            sut.Roll(new Roll(3));

            Assert.IsTrue(sut.Closed);
        }

        [TestMethod]
        public void TenthFrame_Should_Close_After_A_Spare_and_5_Rolls()
        {
            var sut = new TenthFrame();

            sut.Roll(new Roll(3));
            sut.Roll(new Roll(7));
            sut.Roll(new Roll(5));

            Assert.IsTrue(sut.Closed);
        }

        [TestMethod]
        public void Game_Should_Limit_Frames_To_10()
        {
            var sut = new Game();

            // Frame 1
            sut.Roll(10);
            // Frame 2
            sut.Roll(4);
            sut.Roll(6);
            // Frame 3
            sut.Roll(10);
            // Frame 4
            sut.Roll(4);
            sut.Roll(6);
            // Frame 5
            sut.Roll(10);
            // Frame 6
            sut.Roll(4);
            sut.Roll(6);
            // Frame 7
            sut.Roll(10);
            // Frame 8
            sut.Roll(4);
            sut.Roll(6);
            // Frame 9
            sut.Roll(10);
            // Frame 10
            sut.Roll(4);
            sut.Roll(2);

            Assert.IsTrue(sut.GameOver);
        }

        [TestMethod]
        public void Score_Should_Sum_A_Strike_And_Following_Frame()
        {
            var frame1 = new Frame();
            frame1.Roll(new Roll(10));

            var frame2 = new Frame();
            frame2.Roll(new Roll(5));
            frame2.Roll(new Roll(2));

            Assert.AreEqual(17, frame1.Score(new List<Frame>() { frame2 }));
        }

        [TestMethod]
        public void Score_Should_Sum_A_Strike_And_Following_Two_Rolls()
        {
            var frame1 = new Frame();
            frame1.Roll(new Roll(10));

            var frame2 = new Frame();
            frame2.Roll(new Roll(5));
            frame2.Roll(new Roll(2));

            Assert.AreEqual(17, frame1.Score(new List<Frame>() { frame2 }));
        }

        [TestMethod]
        public void TenthFrame_Score_Should_Sum_A_Strike_And_Following_Two_Rolls()
        {
            var frame1 = new TenthFrame();
            frame1.Roll(new Roll(10));
            frame1.Roll(new Roll(5));
            frame1.Roll(new Roll(2));

            Assert.AreEqual(17, frame1.Score(new List<Frame>()));
        }
    }
}
