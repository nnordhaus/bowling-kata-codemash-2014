﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingKata
{
    public class Score
    {
        public int GetTotal(IList<Frame> frames)
        {
            int score = 0;

            for (int i = 0; i < frames.Count; i++)
            {
                score += frames[i].Score(frames.Skip(i+1).Take(2));
            }

            return score;
        }
    }
}